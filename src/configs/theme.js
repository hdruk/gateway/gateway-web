export const theme = {
	colors: {
		grey: '#F6F7F8',
		grey700: '#53575A',
		grey700Alt: '#848E97',
		grey800: '#3C3C3B',
	},
};
